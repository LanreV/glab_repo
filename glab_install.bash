#!/usr/bin/env bash

install_glab (){
    # Add WakeMeOps repository
    curl -sSL "https://raw.githubusercontent.com/upciti/wakemeops/main/assets/install_repository" | sudo bash

    # Install glab
    sudo apt install glab
}
install_glab